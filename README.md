# <a name="back-to-top"></a> Git Cheat Sheet :+1:
This cheat sheet will cover the basic knowledge to equip yourself with the right tools to survive in this confusing world of __Git__. I created this as a reference for you and I when creating future repositories. Now lets get right into it!

## Overview
In this Cheat sheet, there will be two __Areas of focus__, which are:
1. [Markdown](#markdown)
2. [Command Line](#command-line)

<h2 id="markdown">Markdown</h2>

Markdown is a way to style text on the web, and is used by __GitLab__, for example, in the form of ```README.md``` files, which provide different formatting techniques to spice up the instructions and descriptions of their repository and so forth, just like this file! How interesting!

```Tip: you can combine different markdown syntax together``` 

1. [Headers](#headers)
2. [Emphasis](#emphasis)
3. [Lists](#lists)
4. [Links](#links)
5. [Blockquotes](#blockquotes)
6. [Backslash Escapes](#backslash-escapes)
7. [Username mentions](#username-mentions)
8. [Issue References](#issue-references)
9. [Task Lists](#task-lists)
10. [Fenced Code Blocks](#fenced-code-blocks)
11. [Tables](#tables)
12. [Emoji](#emoji)

<h3 id="headers">Headers</h3>

```
# This is an <h1> tag
## This is an <h2> tag
###### This is an <h6> tag
```
<hr />

<h3 id="emphasis">Emphasis</h3>

```
*This text will be italic*
_This will also be italic_

**This text will be bold**
__This will also be bold

*You **can** combine them*
```
<hr />

<h3 id="lists">Lists</h3>

Unordered
```
* Item 1
* Item 2
  * Item 2a
  * Item 2b
```
Ordered
```
1. Item 1
2. Item 2
3. Item 2
  * Item 3a
  * Item 3b	
```
<hr />

<h3 id="links">Links</h3>

Images
```
![Github Logo](/images/logo.png)

Format: ![Alt Text](url)
```

Links (url)
```
http://github.com - automatic!

[GitHub](http://github.com)

Format: [Text](url) or url
```

Links (same page)
```
Take me to [Cheat Sheet](#cheat-sheet)

##### <a name="cheat-sheet"></a> Cheat Sheet

Format: [Text](#name-of-element-seperated-with-dashes)
```
<hr />

<h3 id="blockquotes">Blockquotes</h3>

```
As Kanye West said:

> We're living the future so
> the present is our past
```
As Kanye West said:
> We're living the future so
> the present is our past
<hr />

<h3 id="backslash-escapes">Backslash Escapes</h3>

Markdown allows you to use backslash escapes to generate literal characters which would otherwise have special meaning in Markdowns formatting syntax.
```
\*literal asterisks\*

---would become---
```
\*literal asterisks\*
<hr />

<h3 id="username-mentions">Username mentions</h3>

Typing an ```@``` symbol, followed by a username, will notify that person to come and view the comment. This is called an ```@mention```, because you're mentioning the individual. You can also ```@mention``` teams within an organization.
```
@TanYiXun 

Format: @username
```
@TanYiXun
<hr />

<h3 id="issue-references">Issue References</h3>

Any number that refers to an Issue or Pull Request will be automatically converted into a link. 
```
#1
TanYiXun#1
TanYiXun/git-cheat-sheet#1
```
<hr />

<h3 id="task-lists">Task Lists</h3>

```
- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item
```
- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item
<hr />

<h3 id="fenced-code-blocks">Fenced Code Blocks</h3>

Markdown converts text with four leading spaces into a code block. 
```
    testing 1, 2, 3
```
    testing 1, 2, 3
    
Code blocks can also be created by wrapping your code with three ``` ` ```, in front and behind. Add an optional language identifier and your code with get syntax highlighting.
````
```javascript
function test() {
  console.log("look ma, no spaces");
}
```
````

```javascript
function test() {
  console.log("look ma, no spaces");
}
```

<hr />

<h3 id="tables">Tables</h3>

You can create tables by assembling a list of words and dividing them with hyphens ```-``` (for the first row), and then seperating each column with a pipe ```|```:
```
First Header | Second Header
------------ | -------------
Content cell 1 | Content cell 2
Content column 1 | Content column 2
```
First Header | Second Header
------------ | -------------
Content cell 1 | Content cell 2
Content column 1 | Content column 2
<hr />

<h3 id="emoji">Emoji</h3>

To see a list of every emoji supported, check it out [here](http://www.emoji-cheat-sheet.com).
```
GitLab supports emoji!

:+1: :sparkles: :camel: :tada: :rocket: :metal:
```
GitLab supports emoji!

:+1: :sparkles: :camel: :tada: :rocket: :metal:
<hr />


<h2 id="command-line">Command Line</h2>

The **Git** command line tool is absolutely useful skill as many organisations around the world use **Git** for easy code *maintenance* and *versioning* within big groups of people, especially during projects. Here I will go through step by step the commands required to push your project online.

> This is a simplified version of the command line syntax, but it will be enough for you to starts creating repositories and pushing it to Gitlab!

1. [git init](#git-init)
2. [git status](#git-status)
3. [git add](#git-add)
4. [git commit](#git-commit)
5. [git remote](#git-remote)
6. [git push origin](#git-po)
7. [git pull](#git-pull)
8. [git clone](#git-clone)
9. [git diff](#git-diff)
10. [git reset](#git-reset)


<h3 id="git-init">Git init</h3>

Creates a new local repository with the specified name
```
git init [project-name]
```
Alternatively, you can create the folder with the project name first, then run the init command within the folder
```
mkdir [project-name]
git init
```

<hr />

<h3 id="git-status">Git status</h3>

Lists all new or modified files to be committed
```
git status
```

<hr />

<h3 id="git-add">Git add</h3>

Snapshots the file in preparation for versioning
```
Format: git add [file]

Tip: run "git add ." if you want to snapshot all the files in current directory 
```

<hr />

<h3 id="git-commit">Git commit</h3>

Records file snapshots permanently in version history
```
git commit -m "This is my first commit!"

Format: git commit -m "[descriptive message]"
```

<hr />

<h3 id="git-remote">Git remote</h3>

Add the URL for the remote repository where your local repository will be pushed.
```
git remote add origin "https://gitlab.com/TanYiXun/git-cheat-sheet/"

Format: git remote add origin "[remote repository url]"
```
Verify the new remote URL
```
git remote -v
```

<hr />

<h3 id="git-po">Git push origin</h3>

Push the changes in your local repository to your remote repository, such as on Gitlab, on the **master** branch. After this command, all your committed files will now be online! :tada:
```
git push origin master
```

<hr />

<h3 id="git-pull">Git pull</h3>

Pulls the latest version of the files from your remote repository to your local repository
```
git pull
```
<hr />

<h3 id="git-clone">Git clone</h3>

Downloads a project and its entire version history, which can belong to anyone
```
git clone "https://gitlab.com/TanYiXun/git-cheat-sheet/"

Foramt: git clone "[url]"
```
<hr />

<h3 id="git-diff">Git diff</h3>

Show file differences not yet staged
```
git diff
```
Shows file difference between staging and the last file version
```
git diff --staged
```
<hr />

<h3 id="git-reset">Git reset</h3>

Unstages the file, but preserve its contents
```
git reset [file]
```
<hr />

:point_up_2:[Back to top](#back-to-top)